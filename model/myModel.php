<?php
define('DB_HOST','localhost');
define('DB_USER','root');
define('DB_PASSWD','root');
define('DB_NAME','ai16-secu');

function findUserByLoginPwd($login, $pwd) {
  $mysqli = new mysqli(DB_HOST, DB_USER, DB_PASSWD, DB_NAME);

  if ($mysqli->connect_error) {
      echo 'Erreur connection BDD (' . $mysqli->connect_errno . ') '. $mysqli->connect_error;
      $utilisateur = false;
  } else {
      $req='select nom,prenom,login,id_user,numero_compte,profil_user,solde_compte from users where login="'. $login. '" and mot_de_passe="'. $pwd. '"';
      if (!$result = $mysqli->query($req)) {
          echo 'Erreur requête BDD ['.$req.'] (' . $mysqli->errno . ') '. $mysqli->error;
          $utilisateur = false;
      } else {
          if ($result->num_rows === 0) {
            $utilisateur = false;
          } else {
            $utilisateur = $result->fetch_assoc();
          }
          $result->free();
      }
      $mysqli->close();
  }

  return $utilisateur;

}


function newMessage($from, $to, $subject, $msg) {
  $mysqli = new mysqli(DB_HOST, DB_USER, DB_PASSWD, DB_NAME);

  if ($mysqli->connect_error) {
    echo 'Erreur connection BDD (' . $mysqli->connect_errno . ') '. $mysqli->connect_error;
  } else {
    $request = 'INSERT INTO messages (id_msg, id_user_from, id_user_to, sujet_msg, corps_msg) VALUE ("'. date('YzHisu') .'", "'. $from .'", "'. $to .'", "'. $subject .'", "'. $msg .'")';

    $result = $mysqli->query($request);
    if (!$result) {
      echo 'Erreur requête BDD ['.$request.'] (' . $mysqli->errno . ') '. $mysqli->error;
      return -1;
    }

    if ($result === 0) {
      return -2;
    }

    $mysqli->close();
    return 0; 
  }
}


function getAllMessages() {
  $mysqli = new mysqli(DB_HOST, DB_USER, DB_PASSWD, DB_NAME);

  if ($mysqli->connect_error) {
    echo 'Erreur connection BDD (' . $mysqli->connect_errno . ') '. $mysqli->connect_error;
  } else {
    $request = 'SELECT id_user_from AS Emeteur, sujet_msg AS Sujet, corps_msg AS Corps FROM messages WHERE id_user_to = "'. $_SESSION["connected_user"]["id_user"] .'"';

    $result = $mysqli->query($request);
    if (!$result) {
      echo 'Erreur requête BDD ['.$request.'] (' . $mysqli->errno . ') '. $mysqli->error;
      return -1;
    }

    $msgList = mysqli_fetch_all($result, MYSQLI_ASSOC);
    $result->free();
    $mysqli->close();
    return $msgList;    
  }
}


function getAllAccountsNumber() {
  $mysqli = new mysqli(DB_HOST, DB_USER, DB_PASSWD, DB_NAME);

  if ($mysqli->connect_error) {
    echo 'Erreur connection BDD (' . $mysqli->connect_errno . ') '. $mysqli->connect_error;
  } else {
    $request = 'SELECT id_user, nom, numero_compte, solde_compte FROM user';

    $result = $mysqli->query($request);
    if (!$result) {
      echo 'Erreur requête BDD ['.$request.'] (' . $mysqli->errno . ') '. $mysqli->error;
      return -1;
    }

    $accountList = mysqli_fetch_all($result, MYSQLI_ASSOC);
    $result->free();
    $mysqli->close();
    return $accountList;    
  }
}

function getAccountNumber($user) {
  $mysqli = new mysqli(DB_HOST, DB_USER, DB_PASSWD, DB_NAME);

  if ($mysqli->connect_error) {
    echo 'Erreur connection BDD (' . $mysqli->connect_errno . ') '. $mysqli->connect_error;
  } else {
    $request = 'SELECT numero_compte FROM user WHERE id_user = "'. $user .'"';

    $result = $mysqli->query($request);
    if (!$result) {
      echo 'Erreur requête BDD ['.$request.'] (' . $mysqli->errno . ') '. $mysqli->error;
      return -1;
    }

    $num_account = mysqli_fetch_all($result, MYSQLI_ASSOC);
    $result->free();
    $mysqli->close();
    return strval(implode($num_account[0]));    
  }
}


function getAccountAmount($user) {
  $mysqli = new mysqli(DB_HOST, DB_USER, DB_PASSWD, DB_NAME);

  if ($mysqli->connect_error) {
    echo 'Erreur connection BDD (' . $mysqli->connect_errno . ') '. $mysqli->connect_error;
  } else {
    $request = 'SELECT solde_compte FROM user WHERE id_user = "'. $user .'"';

    $result = $mysqli->query($request);
    if (!$result) {
      echo 'Erreur requête BDD ['.$request.'] (' . $mysqli->errno . ') '. $mysqli->error;
      return -1;
    }

    $solde_compte = mysqli_fetch_all($result, MYSQLI_ASSOC);
    $result->free();
    $mysqli->close();
    return strval(implode($solde_compte[0]));    
  }
}


function customerList(){
  $mysqli = new mysqli(DB_HOST, DB_USER, DB_PASSWD, DB_NAME);

  if ($mysqli->connect_error) {
    echo 'Erreur connection BDD (' . $mysqli->connect_errno . ') '. $mysqli->connect_error;
  } else {
    $request = 'SELECT * FROM user WHERE profil_user = "CLIENTT"';

    $result = $mysqli->query($request);
    if (!$result) {
      echo 'Erreur requête BDD ['.$req.'] (' . $mysqli->errno . ') '. $mysqli->error;
      return -1;
    }

    $customerList = mysqli_fetch_all($result, MYSQLI_ASSOC);
    $result->free();
    $mysqli->close();
    return $customerList;
  }
}


function transfer($to, $amount) {
  $mysqli = new mysqli(DB_HOST, DB_USER, DB_PASSWD, DB_NAME);

  if ($mysqli->connect_error) {
    echo 'Erreur connection BDD (' . $mysqli->connect_errno . ') '. $mysqli->connect_error;
  } else {
    $requestFrom = 'UPDATE user SET solde_compte = solde_compte-'. $amount .' WHERE id_user = "'. $_SESSION["connected_user"]["id_user"] .'"';
    $requestTo = 'UPDATE user SET solde_compte = solde_compte+'. $amount .' WHERE id_user = "'. $to .'"';

    $result = $mysqli->query($requestFrom);
    if (!$result) {
      echo 'Erreur requête BDD ['.$requestFrom.'] (' . $mysqli->errno . ') '. $mysqli->error;
      return -1;
    }

    $result = $mysqli->query($requestTo);
    if (!$result) {
      echo 'Erreur requête BDD ['.$requestTo.'] (' . $mysqli->errno . ') '. $mysqli->error;
      return -2;
    }
  }

  $mysqli->close();
  return 0;
}
?>
