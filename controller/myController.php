<?php
  require ('model/myModel.php');

  function isLoggedOn() {
      if(!isset($_SESSION["connected_user"]) || $_SESSION["connected_user"] == "") {
          // utilisateur non connecté
          require('view/login.php');
          return false;
      }
      return true;
  }

  function badaction() {
    require('view/erraction.php');
  }

  function authenticate($login, $mdp) {
      if ($login == "" || $mdp == "") {
          // manque login ou mot de passe
          $errmsg = "nullvalue";
          require('view/login.php');
      } else {
          $utilisateur = findUserByLoginPwd($login, $mdp);
          if ($utilisateur == false) {
            // echec authentification
            require('view/errauthent.php');
          } else {
            $_SESSION["connected_user"] = $utilisateur;
            require('view/accueil.php');
          }
      }
  }

  function home() {
      if (isLoggedOn()) {
        require('view/accueil.php');
      }
  }

  function clients() {
      if (isLoggedOn()) {
          require('view/ficheClient.php');
      }
  }

  function messages() {
      if (isLoggedOn()) {
          require('view/messagerie.php');
      }
  }

  function sendmessages($destinataire, $sujet, $message) {
    if ($destinataire== "") {
    // manque le destinataire
    $errmsg = "nullvalue";
    require('view/messagerie.php');
  } else {
    $mess_statut=newMessage($_SESSION["connected_user"]['id_user'], $destinataire, $sujet, $message);

    switch ($mess_statut) {
      case 0:
        $msg = "Message envoyé";
        break;

      case -1:
        $msg = "Message non envoyé (erreur requête)";
        break;
      
      default:
        $msg = "Message non envoyé";
        break;
    }
    ?> <script type="text/javascript">alert(<?php echo "'".$msg."'"; ?>)</script>  <?php

    setcookie("msg_alert",$msg, 60*60*24, "/");
    require('view/messagerie.php');
    }
  }

  function NouveauVirement($Beneficiaire,$Montant) {
    $res = transfer($Beneficiaire, $Montant);

    switch ($res) {
      case 0:
        $msg = 'Virement de '.$Montant.'euros Effectué';
        break;
      case -1:
        $msg = 'Virement de '.$Montant.'euros non effectué (emetteur)';
        break;
      case -2:
        $msg = 'Virement de '.$Montant.'euros non effectué (recepteur)';
        break;
      
      default:
        $msg = 'Echec du virement';
        break;
    }

    ?> <script type="text/javascript">alert(<?php echo "'".$msg."'"; ?>)</script>  <?php

    setcookie("msg_alert",$msg, 60*60*24, '/');
    
    require('view/accueil.php');
  }

  function ListeClients() {
    $clients = customerList();
    require('view/ficheClient.php');
  }

  function virements() {
      if (isLoggedOn()) {
          require('view/virement.php');
      }
  }

  function disconnect() {
    unset($_SESSION["connected_user"]);
    require('view/login.php');
  }

?>
