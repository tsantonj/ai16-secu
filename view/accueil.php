<!doctype html>
<html lang="fr">
<head>
  <meta charset="utf-8">
  <title>Accueil</title>
  <link rel="stylesheet" type="text/css" media="all"  href="/public/css/mystyle.css" />
</head>
<body>
  <header>
    <h1>Accueil Banque en ligne</h1>
  </header>
  <main>
    <article>
      <header>
        <h2>Bienvenue <?php echo $_SESSION["connected_user"]["prenom"];?> <?php echo $_SESSION["connected_user"]["nom"];?></h2>

        <?php
          if (isset($_COOKIE['msg_alert']) ){
            echo '<h3>'.$_COOKIE['msg_alert'].'</h3>';

            /*?>
              <script type="text/javascript">alert(<?php $msg ?>)</script>
            <?php*/
          }
        ?>
      </header>
      <div class="form">
        <form method="GET" action="/index.php" >
          <input type="hidden" name="action" value="messagerie">
          <button>Messagerie</button>
        </form>
        <form method="GET" >
          <input type="hidden" name="action" value="virement">
          <input type="hidden" name="numero_compte" value="<?php echo $_SESSION["connected_user"]["numero_compte"];?>">
          <button>Virement</button>
        </form>
<?php if ($_SESSION["connected_user"]["profil_user"] == "ADMIN") { ?>
        <form method="GET" >
          <input type="hidden" name="action" value="clients">
          <button>Clients</button>
        </form>
<?php } ?>
        <form method="GET" >
          <input type="hidden" name="action" value="disconnect">
          <button>Déconnexion</button>
        </form>
      </div>
    </article>
  </main>
</body>
</html>
