<!doctype html>
<html lang="fr">
<head>
  <meta charset="utf-8">
  <title>Virement</title>
  <link rel="stylesheet" type="text/css" media="all"  href="/public/css/mystyle.css" />
</head>
<body>
  <header>
    <h2>Virements (User connecté : <?php echo $_SESSION["connected_user"]["prenom"].' '.$_SESSION["connected_user"]["nom"];?>)</h2>
  </header>
  <main>
    <?php
      $accountList = getAccountNumber($_SESSION["connected_user"]["id_user"]);
      $accountAmount = getAccountAmount($_SESSION["connected_user"]["id_user"]);
    ?>
    <article>
      <div class="form">
        <?php
        if ($accountList != -1) {
        ?>
          <h3>Solde de votre compte <?php echo $accountAmount . ' euros'; ?></h3>
            <form method="POST" action="./index.php">
              <input type="hidden" name="action" value="NouveauVirement">
              <select name="Beneficiaire" placeholder="Beneficiaire">
                  <?php 
                  $accountList = getAllAccountsNumber();
                  foreach ($accountList as $item) echo "<option value='".$item["id_user"]."'>". $item["nom"] ." - ". $item["id_user"] ."</option>"?>
              </select>
              <br />
              <br />
              <input type="number" name="Montant" placeholder="Montant"/>
              <button>Valider le virement</button>
            </form>
        <?php
        } else {
        ?>
          <h3>ERROR</h3>
        <?php } ?>
      </div>
      <div class="form">
        <form method="GET" action="/index.php" >
          <input type="hidden" name="action" value="home">
          <button>Accueil</button>
        </form>
      </div>
    </article>
  </main>
</body>
</html>
