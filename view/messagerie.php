<!doctype html>
<html lang="fr">
<head>
  <meta charset="utf-8">
  <title>Messages</title>
  <link rel="stylesheet" type="text/css" media="all"  href="/public/css/mystyle.css" />
</head>
<body>
  <header>
    <h2>Messages (User connecté : <?php echo $_SESSION["connected_user"]["prenom"].' '.$_SESSION["connected_user"]["nom"];?>)</h2>
  </header>
  <main>
    <?php $accountList = getAllAccountsNumber(); ?>
    <?php $messageList = getAllMessages(); ?>
    <article>
      <div class="form">
        <table BORDER="1">
          <CAPTION> Messages </caption>
          <tr>
              <td> Emeteur </td>
              <td> Sujet </td>
              <td> Message </td>
          </tr>
          <?php 
            $messageList = getAllMessages();
            foreach ($messageList as $item) echo "<tr><td>".$item["Emeteur"]."</td><TD>". $item["Sujet"] ."</TD><TD>". $item["Corps"] ."</TD></tr>"?>
        </table>
      </div>
      <div class="form">
        <?php
        if (isset($errmsg) && $errmsg == "nullvalue") {
        echo '<p class="errmsg">Saisissez le destinataire</p>';
        }
        ?>
        <form metdod="GET" action="/index.php">
          <input type="hidden" name="action" value="sendmessages">
          <!--<input type="text" name="destinataire" placeholder="id destinataire"/>-->
          <select name="destinataire" placeholder="Destinataire">
          <?php 
            $accountList = getAllAccountsNumber();
            foreach ($accountList as $item) echo "<option value='".$item["id_user"]."'>". $item["nom"] ." - ". $item["id_user"] ."</option>"?>
          </select>
          <br />
          <br />
          <input type="text" name="sujet" placeholder="Sujet"/>
          <textarea rows="4" cols="40" name="message" placeholder="Votre message"></textarea>
          <br />
          <br />
          <button>Envoyer</button>
        </form>
      </div>
    </article>

    <article>
      <div class="form">
        <form method="GET" action="/index.php" >
          <input type="hidden" name="action" value="home">
          <button>Accueil</button>
        </form>
      </div>
    </article>
  </main>
</body>
</html>
