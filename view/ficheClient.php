<!doctype html>
<html lang="fr">
<head>
  <meta charset="utf-8">
  <title>Clients</title>
  <link rel="stylesheet" type="text/css" media="all"  href="/public/css/mystyle.css" />
</head>
<body>
  <header>
    <h2>Clients (User connecté : <?php echo $_SESSION["connected_user"]["prenom"].' '.$_SESSION["connected_user"]["nom"];?>)</h2>
  </header>
  <main>
      <?php
        foreach ($clients as $client) {
          echo "<article><div class=\"form\">";
          echo "<h3>".$client["nom"]."</h3>";
          echo '<p>Compte numéro:'.$client["numero_compte"].' Solde: '.$client["solde_compte"].'</p>';
          echo "</div></article>";
        }
      ?>
    <article>
      <div class="form">
        <form method="GET" action="/index.php" >
          <input type="hidden" name="action" value="home">
          <button>Accueil</button>
        </form>
      </div>
    </article>
  </main>
</body>
</html>
