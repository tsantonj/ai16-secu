<?php
  session_start();

  require('controller/myController.php');

  if (isset($_GET['action'])) {
      if ($_GET['action'] == 'authenticate') {
          authenticate($_GET['login'],$_GET['mdp']);
      } else if ($_GET['action'] == 'home') {
          home();
      } else if ($_GET['action'] == 'messagerie') {
          messages();
      } else if ($_GET['action'] == 'virement') {
          virements();
      } else if ($_GET['action'] == 'clients') {
          clients();
      } else if ($_GET['action'] == 'disconnect') {
          disconnect();
      } else if ($_GET['action'] == 'retour') {
          header('Location: '.$_GET['target']);
      } else if ($_GET['action'] == 'sendmessages') {
          sendmessages($_GET['destinataire'], $_GET['sujet'], $_GET['message']);
      } else {
          badaction();
      }
  }  else {
      if (isset($_POST['action'])) {
        if ($_POST['action'] == 'NouveauVirement') {
          NouveauVirement($_POST['Beneficiaire'],$_POST['Montant']);
        }
      } else {
        // aucune action => accueil
        home();
      }
    }

?>
